<?php

/*{{{ v.151005.001 (0.0.2)

	Sample config file for bitbucket hooks.

	Based on 'Automated git deployment' script by Jonathan Nicoal:
	http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

	See README.md and config.sample.php

	---
	Igor Lilliputten
	mailto: igor at lilliputten dot ru
	http://lilliputtem.ru/

	Ivan Pushkin
	mailto: iv dot pushk at gmail dot com

}}}*/

/*{{{ Auxiliary variables, used only for constructing $CONFIG and $PROJECTS  */

$REPOSITORIES_PATH = '/data/www/deploy/projects/';
$PROJECTS_PATH     = '/data/www/deploy/projects/';

/*}}}*/

// Base tool configuration:
$CONFIG = array(
	'bitbucketUsername' => 'pixtig', // The username or team name where the
	// repository is located on bitbucket.org, *REQUIRED*

	'gitCommand'       => 'git',              // Git command, *REQUIRED*
	'repositoriesPath' => $REPOSITORIES_PATH, // Folder containing all repositories, *REQUIRED*
	'log'              => true,               // Enable logging, optional
	'logFile'          => 'bitbucket.log',    // Logging file name, optional
	'logClear'         => true,               // clear log each time, optional
	'verbose'          => true,               // show debug info in log, optional
	'folderMode'       => 0700,               // creating folder mode, optional
	'mailFrom'	       => 'Automatic Bitbucket Deploy <git@bitbucket.com>',// The sender e-mail address for info emails
);

// List of deploying projects:
$PROJECTS = array(
	'geducar-versi-n-2' => array( // The key is a bitbucket.org repository name
		'master' => array(
			'deployPath'  => $PROJECTS_PATH.'/master', // Path to deploy project, *REQUIRED*
			'postHookCmd' => '',                // command to execute after deploy, optional
			'mailTo'      => ''            // log email recipient, optional
		),
		'prueba' => array(
			'deployPath'  => $PROJECTS_PATH.'/prueba', // Path to deploy project, *REQUIRED*
			'postHookCmd' => '',                // command to execute after deploy, optional
			'mailTo'      => ''            // log email recipient, optional
		),
		'live' => array(
			'deployPath'  => $PROJECTS_PATH.'/live', // Path to deploy project, *REQUIRED*
			'postHookCmd' => '',                // command to execute after deploy, optional
			'mailTo'      => ''            // log email recipient, optional
		),
	),

);